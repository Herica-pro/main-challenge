//
//  TelaDasPalavras.swift
//  Xprize1App
//
//  Created by Felipe Lukavei Ferreira on 10/22/15.
//  Copyright © 2015 Felipe Lukavei Ferreira. All rights reserved.
//

import SpriteKit
import AVFoundation
class TelaDasPalavras: SKScene {
    
    var arrayLetras: Array<SKSpriteNode> = []
    var arrayEspacos: Array<SKSpriteNode> = []
    
    var audioPlayer: AVAudioPlayer!

    var touchTracker : [UITouch : SKNode] = [:]
    var posicaoInicialDoPonto = CGPoint(x: 0, y: 0)
    
    //TODO: SOM DE SILABAS
    
    enum Layer: CGFloat {
        case Background
        case letraMarcaDagua
        case letraMovel
        case Botoes

    }
    enum Names: String {
        case backGround = "backGround"
        case Player = "player"
        case letraMarcaDagua = "letraMarcaDagua"
        case letraMovel = "letraMovel"
    }

    override func didMoveToView(view: SKView) {
        let palavra = importarPalavras()
        montarMarcaDagua(palavra)
        montarLetras(palavra)
    }
    
    override func update(currentTime: NSTimeInterval) {
        //teste de encaixe das palavras

    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let location = touches.first!.locationInNode(self)

        if posicaoInicialDoPonto == CGPoint (x: 0, y: 0)
        {
        posicaoInicialDoPonto = location
        }
        for node in arrayLetras
        {
            if node.containsPoint(location)
            {
                if let letra = node.name?.characters.last
                {
                    soltaOSom("\(letra)")
                }
            }
        }

    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches
        {
            let location = touch.locationInNode(self)
            for node in arrayLetras
            {
                if node.containsPoint(location)
                {
                    if node.name!.containsString(Names.letraMovel.rawValue)
                    {
                    node.position = location
                    break
                    }
                }
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let location = touches.first!.locationInNode(self)
        if !arrayLetras.isEmpty
        {
            for index in 0 ... arrayLetras.count - 1
            {
                let node = arrayLetras[index]
                
                if node.containsPoint(location)
                {
                    for indexEspaco in 0 ... arrayEspacos.count - 1
                    {
                        let espaco = arrayEspacos[indexEspaco]
                        
                        if espaco.containsPoint(location)
                        {
                            if let nomeDoNode = node.name
                            {
                                if let nomeDoEspaco = espaco.name
                                {
                                    if nomeDoNode.containsString(nomeDoEspaco)
                                    {
                                        node.position = CGPoint(x: espaco.position.x + espaco.size.width/2, y: espaco.position.y)
                                        node.zRotation = espaco.zRotation
                                        print("arrayLetras: \(arrayLetras.count) index:\(index); arrayEspacos: \(arrayEspacos.count) index: \(indexEspaco)")
                                        arrayLetras.removeAtIndex(index)
                                        arrayEspacos.removeAtIndex(indexEspaco)
                                        print("removeu")
                                        if arrayLetras.isEmpty
                                        {
                                            NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "palavraPronta", userInfo: nil, repeats: false)
                                        }
                                        return
                                    }
                                    else if posicaoInicialDoPonto != CGPoint(x: 0, y: 0)
                                    {
                                        let tempo = (posicaoInicialDoPonto.x - node.position.x) * (posicaoInicialDoPonto.x - node.position.x) + (posicaoInicialDoPonto.y - node.position.y) * (posicaoInicialDoPonto.y - node.position.y)
                                        let mover = SKAction.moveTo(posicaoInicialDoPonto, duration: NSTimeInterval(sqrt(tempo)/1000))
                                        node.runAction(mover)
                                        posicaoInicialDoPonto = CGPoint(x: 0, y: 0)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        
    }
    
    func importarPalavras ()-> Array<String>
    {
        print(myDictionaries.palavras)
        if myDictionaries.palavras["arrayEmUso"]!.isEmpty
        {
            myDictionaries.palavras["arrayEmUso"] = myDictionaries.palavras["arrayPadrao"]
        }
        if let palavras = myDictionaries.palavras["arrayEmUso"]
        {
            if let letrasDaPalavra = myDictionaries.palavras[palavras[0]]
            {
                myDictionaries.palavras["arrayEmUso"]?.removeAtIndex((myDictionaries.palavras["arrayEmUso"]?.indexOf(palavras[0]))!)
                
                return letrasDaPalavra
            }
        }
        return ["ERROR: Palavra Não carregada"]
    }
    
    func montarMarcaDagua (palavra: Array<String>)
    {
        let quantidadeDeLetras = palavra.count
        let tamanho = CGSize(width: self.size.width / CGFloat(quantidadeDeLetras + 1), height: self.size.height / 7)
        var posicaoDaLetraAtual = 0
        let posicaoFixa = self.size.width * CGFloat(1 - 1 / Float(quantidadeDeLetras)) / CGFloat (quantidadeDeLetras * (quantidadeDeLetras + 1))
        for letra in palavra
        {
            let posicao = CGPoint(x: (posicaoFixa * CGFloat(posicaoDaLetraAtual + 1)) + tamanho.width * CGFloat(posicaoDaLetraAtual), y: self.size.height/2)
            criarLetrasMarcaDagua(letra, posicao: posicao, tamanho: tamanho)
            posicaoDaLetraAtual = posicaoDaLetraAtual + 1
        }
    }
    
    func criarLetrasMarcaDagua (letra: String, posicao: CGPoint, tamanho: CGSize)
    {
        var imagemDaLetra = SKSpriteNode(imageNamed: "PHMarcaDagua.png")
        if let _ = UIImage(named: "\(letra)ma.png")
        {
            imagemDaLetra = SKSpriteNode(imageNamed: "\(letra)ma.png")
        }
        print(posicao)
        imagemDaLetra.position = posicao
        imagemDaLetra.zPosition = Layer.letraMarcaDagua.rawValue
        imagemDaLetra.name = letra
        imagemDaLetra.anchorPoint = CGPoint(x: 0, y: 0.5)
        imagemDaLetra.size = tamanho
        
        arrayEspacos.append(imagemDaLetra)
        self.addChild(imagemDaLetra)
    }
    func montarLetras (var palavra: Array<String>)
    {
        let quantidadeDeLetras = palavra.count
        let tamanho = CGSize(width: self.size.width / CGFloat(quantidadeDeLetras + 1), height: self.size.height / 7)
        let posicaoFixa = self.size.width * CGFloat(1 - 1 / Float(quantidadeDeLetras)) / CGFloat (quantidadeDeLetras * (quantidadeDeLetras + 1))
        var posicaoDaLetra = 0
        for _ in 0 ... (quantidadeDeLetras - 1)
        {
            let posicaoDaLetraAtual = Int(arc4random_uniform(UInt32(palavra.count)))
            
            let posicao = CGPoint(x: (posicaoFixa * CGFloat(posicaoDaLetra + 1)) + tamanho.width * 0.5 + tamanho.width * CGFloat(posicaoDaLetra), y: self.size.height/4 + CGFloat(posicaoDaLetra%2) * self.size.height/2)
            criarLetras(palavra[posicaoDaLetraAtual], posicao: posicao, tamanho: tamanho)
            posicaoDaLetra++
            print(palavra[posicaoDaLetraAtual])
            palavra.removeAtIndex(posicaoDaLetraAtual)
        }
        
    }
    
    func criarLetras (letra: String, posicao: CGPoint, tamanho: CGSize)
    {
        var imagemDaLetra = SKSpriteNode(imageNamed: "PHLetra.png")
        if let _ = UIImage(named: "\(letra).png")
        {
            imagemDaLetra = SKSpriteNode(imageNamed: "\(letra).png")
        }
        print(posicao)
        imagemDaLetra.position = posicao
        imagemDaLetra.zPosition = Layer.letraMovel.rawValue
        imagemDaLetra.name = "\(Names.letraMovel.rawValue)_\(letra)"
        imagemDaLetra.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        imagemDaLetra.size = tamanho
        
        var rotacao = M_PI / 180
        let posOUneg = Double (arc4random_uniform(90))
        if posOUneg < 45
        {
            rotacao = -rotacao * posOUneg
        }
        else if posOUneg == 45
        {
            rotacao = 0
        }
        else if posOUneg > 45
        {
            rotacao = rotacao * (posOUneg - 45)
        }
        print("rotação: \(rotacao) posOUneg: \(posOUneg)")
        imagemDaLetra.zRotation = CGFloat(rotacao)
        
        arrayLetras.append(imagemDaLetra)
        
        self.addChild(imagemDaLetra)
        
    }
    
    func soltaOSom (letraDoSom: String)
    {
        if let path: NSURL = NSBundle.mainBundle().URLForResource(letraDoSom, withExtension: "mp3")
        {
            do {
                try audioPlayer = AVAudioPlayer(contentsOfURL: path)
            } catch _{
                print("NO AUDIO PLAYER")
            }
            audioPlayer?.numberOfLoops = 0
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
        }
        
    }
    
    func palavraPronta ()
    {
        let scene = GameScene(size: self.size)
        let transition = SKTransition.crossFadeWithDuration(1.0)
        scene.scaleMode = SKSceneScaleMode.AspectFill
        self.scene!.view?.presentScene(scene, transition: transition)
    }
}
