//
//  GameScene.swift
//  Xprize1App
//
//  Created by Felipe Lukavei Ferreira on 9/9/15.
//  Copyright (c) 2015 Felipe Lukavei Ferreira. All rights reserved.
//


import SpriteKit



class GameScene: SKScene {

    
    var jachamou = false
    var arrayPalavras : Array<String>?
    var personagem : SKSpriteNode = SKSpriteNode(color: UIColor.orangeColor(), size: CGSizeMake(70, 70))
    enum Layer: CGFloat {
        case Background
        case Player
        case Botoes

    }
    enum Names: String {
        case backGround = "backGround"
        case Player = "player"
        case botaoGame1 = "goGame"
        case botaoGame2 = "goGame2"
        case botaoPalavra = "botaoPalavra"
        case botaoTakePhoto = "botaoTakePhoto"
    }

    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        addBackground()
        addBotaoGoGame()
        addBotaoTirarFoto()
        addPlayer()
        DataManager.saveUserData()
    }
    

        //MARK: TOUCHING
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches 
        let location = touch.first!.locationInNode(self)
        let node = self.nodeAtPoint(location)
        // If next button is touched, start transition to second scene
        

        
        if (node.name == Names.botaoGame1.rawValue) {
            let aleatoria = arc4random_uniform(2)
            if aleatoria<2
            {
                let secondScene = Game1(size: self.size)
                let transition = SKTransition.flipVerticalWithDuration(1.0)
                secondScene.scaleMode = SKSceneScaleMode.AspectFit
                self.scene!.view?.presentScene(secondScene, transition: transition)
            }
        }
        else if node.name == Names.botaoGame2.rawValue
        {
            let secondScene = Game2(size: self.size)
            let transition = SKTransition.flipVerticalWithDuration(1.0)
            secondScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(secondScene, transition: transition)
        }
        else if node.name == Names.botaoPalavra.rawValue
        {
            let scene = TelaDasPalavras(size: self.size)
            let transition = SKTransition.flipVerticalWithDuration(1.0)
            scene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(scene, transition: transition)
        }
        else if node.name == Names.botaoTakePhoto.rawValue
        {
            takePhoto()
        }
        else // SE NÃO TOCAR EM NENHUM DESTES NÓ ACIMA
        {
            
//            for jogador in self.children
//            {
//                if jogador.name == Names.Player.rawValue
//                {
                    var velocity = (location.x - personagem.position.x)/250
                    if velocity < 0
                    {
                        velocity = -velocity
                    }
                    let moveTo = SKAction.moveToX(location.x, duration: NSTimeInterval(velocity))
                    personagem.runAction(moveTo)
//                }
//            }
        }
    }
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        if personagem.position.x > self.size.width * 0.9
        {
            let scene = TelaDasPalavras(size: self.size)
            let transition = SKTransition.flipVerticalWithDuration(1.0)
            scene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(scene, transition: transition)
        }

    }
    

    
    //MARK: CRIAR VIEW
    
    func addBackground ()
    {
        let background = SKSpriteNode(imageNamed: "background.jpg")
        
        let proporcaoDeTamanho = background.size.width/background.size.height
        background.size.height = self.frame.size.height
        background.size.width = background.size.height * proporcaoDeTamanho
        background.anchorPoint = CGPoint(x: 0, y: 0)
        background.position = CGPoint(x: 0, y: 0)
        background.name = Names.backGround.rawValue
        background.zPosition = Layer.Background.rawValue
        self.addChild(background)
    }

    func addBotaoGoGame ()
    {
        let button = SKSpriteNode(color: UIColor.lightGrayColor(), size: CGSizeMake(100, 100))
        button.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        button.name = Names.botaoGame1.rawValue
        button.zPosition = Layer.Botoes.rawValue
        self.addChild(button)
        
        let buttonD = SKSpriteNode(color: UIColor.redColor(), size: CGSizeMake(100, 100))
        buttonD.position = CGPointMake(self.frame.size.width/2, self.frame.size.height*0.65)
        buttonD.name = Names.botaoGame2.rawValue
        buttonD.zPosition = Layer.Botoes.rawValue
        self.addChild(buttonD)
        
        let buttonP = SKSpriteNode(color: UIColor.purpleColor(), size: CGSizeMake(100, 100))
        buttonP.position = CGPointMake(self.frame.size.width/2, self.frame.size.height*0.8)
        buttonP.name = Names.botaoPalavra.rawValue
        buttonP.zPosition = Layer.Botoes.rawValue
        self.addChild(buttonP)
    }
    
    
    func addBotaoTirarFoto ()
    {
        let button = SKSpriteNode(color: UIColor.lightGrayColor(), size: CGSizeMake((self.frame.size.width*0.1), self.frame.size.width*0.1))
        button.anchorPoint = CGPoint(x: 1, y: 0)
        button.name = Names.botaoTakePhoto.rawValue
        button.zPosition = Layer.Botoes.rawValue
        button.position = CGPointMake(self.size.width - 25, 25)
        self.addChild(button)
    }
    
    func takePhoto ()
    {
        self.view?.window?.rootViewController?.performSegueWithIdentifier("PresentPhotoView", sender: nil)
    }
    
    func addPlayer ()
    {
        personagem.position = CGPointMake(self.size.width / 3, self.size.height * 0.15)
        personagem.name = Names.Player.rawValue
        personagem.zPosition = Layer.Player.rawValue
        if let imagem = loadImageFromPath(fileInDocumentsDirectory("/imagemPersonagem")) as UIImage!
        {
            personagem.texture = SKTexture(image: imagem.imageRotatedByDegrees(180, flip: false))
        }
        self.addChild(personagem)
    }
    //MARK: METODOS SUPORTE
    
    
    func loadImageFromPath(path: String) -> UIImage? {
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            print("missing image at: \(path)")
        }
        return image
        
    }
    func documentsDirectory() -> String {
        let documentsFolderPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] 
        return documentsFolderPath
    }
    // Get path for a file in the directory
    
    func fileInDocumentsDirectory(filename: String) -> String {
        return documentsDirectory().stringByAppendingString(filename)
    }
    

}
