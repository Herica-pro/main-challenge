//
//  Game1.swift
//  Xprize1App
//
//  Created by Felipe Lukavei Ferreira on 9/9/15.
//  Copyright (c) 2015 Felipe Lukavei Ferreira. All rights reserved.
//

import SpriteKit
import AVFoundation

class Game1: SKScene {
    
    var jogoComecou = false
    var acertos = 0
    var vidas = 3
    var tempoAtual = CFTimeInterval()
    var arrayQueVaiPiscar = Array<[String]>()
    var podePiscar = false
    var letraCorrente = 0
    var correndoSequencia = 0
    var vezDoJogadorRepetirPadrao = false
    var audioPlayer: AVAudioPlayer!
    
    var arrayDeBotoes = Array <SKSpriteNode>()
    var arrayDeLabelNodes = Array <SKLabelNode>()
    enum Layer: CGFloat {
        case Background
        case Player
        case Botoes
    }
    enum Names: String {
        case botaoPreviousScreen = "botaoPreviousScreen"
        case backGround = "backGround"
        case Player = "player"
        case botoesAlternativas = "btAlternativas"
        case botaoPlayGame = "playGameButton"
    }
    
    
    //MARK: ENTRADA DE DADOS
    

    
    func montarArray() -> Array<[String]> //NESTE MÉTODO, É CRIADA A ENTRADA DA SEQUENCIA QUE SERÁ REPETIDA, CADA POSIÇÃO DO ARRAY É UMA COMBINAÇÃO
    {
        let letrasConhecidas = myDictionaries.letrasConhecidas
        
        let letra1 = Int(arc4random_uniform(UInt32(letrasConhecidas.count)))
        var letra2 = Int(arc4random_uniform(UInt32(letrasConhecidas.count)))
        var letra3 = Int(arc4random_uniform(UInt32(letrasConhecidas.count)))
        while letra2 == letra1
        {
            letra2 = Int(arc4random_uniform(UInt32(letrasConhecidas.count)))
        }
        while letra3 == letra2 || letra3 == letra1
        {
            letra3 = Int(arc4random_uniform(UInt32(letrasConhecidas.count)))
        }

        let arrayDeLetrasUsadas = [letrasConhecidas[letra1], letrasConhecidas[letra2], letrasConhecidas[letra3]]
        let posicoes = [arrayDeLetrasUsadas[0], arrayDeLetrasUsadas[1], arrayDeLetrasUsadas[2]]
        let combinacao1 = [arrayDeLetrasUsadas[Int(arc4random_uniform(3))], arrayDeLetrasUsadas[Int(arc4random_uniform(3))], arrayDeLetrasUsadas[Int(arc4random_uniform(3))], arrayDeLetrasUsadas[Int(arc4random_uniform(3))]]
        let combinacao2 = [arrayDeLetrasUsadas[Int(arc4random_uniform(3))], arrayDeLetrasUsadas[Int(arc4random_uniform(3))], arrayDeLetrasUsadas[Int(arc4random_uniform(3))], arrayDeLetrasUsadas[Int(arc4random_uniform(3))], arrayDeLetrasUsadas[Int(arc4random_uniform(3))]]
        let combinacao3 = [arrayDeLetrasUsadas[Int(arc4random_uniform(3))], arrayDeLetrasUsadas[Int(arc4random_uniform(3))], arrayDeLetrasUsadas[Int(arc4random_uniform(3))],arrayDeLetrasUsadas[Int(arc4random_uniform(3))],arrayDeLetrasUsadas[Int(arc4random_uniform(3))],arrayDeLetrasUsadas[Int(arc4random_uniform(3))]]

//        let arrayDeLetras = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","X","W","Y","Z"]
        var arrayDeArrays = Array<[String]>()
        arrayDeArrays.append(posicoes)
        arrayDeArrays.append(combinacao1)
        arrayDeArrays.append(combinacao2)
        arrayDeArrays.append(combinacao3)
        
        print("arrayDeArrays:\(arrayDeArrays)", terminator: "")
        return arrayDeArrays
    }
    
    //MARK: MONTAR TELA
    override func didMoveToView(view: SKView)
    {
        self.backgroundColor = SKColor(red: 0.15, green:0.15, blue:0.3, alpha: 1.0)
        addBotaoVoltar()
        addBotaoPlayGame()
    }
    
    func criarBotoes(Letra1: String, Letra2: String, Letra3: String)
    {
        var arrayDeLetras = Array<String>()
        let aleatorio = arc4random_uniform(6)
        switch aleatorio
        {
        case 0:
            arrayDeLetras.append(Letra1)
            arrayDeLetras.append(Letra2)
            arrayDeLetras.append(Letra3)
            break
        case 1:
            arrayDeLetras.append(Letra1)
            arrayDeLetras.append(Letra3)
            arrayDeLetras.append(Letra2)
            break
        case 2:
            arrayDeLetras.append(Letra2)
            arrayDeLetras.append(Letra1)
            arrayDeLetras.append(Letra3)
            break
        case 3:
            arrayDeLetras.append(Letra2)
            arrayDeLetras.append(Letra3)
            arrayDeLetras.append(Letra1)
            break
        case 4:
            arrayDeLetras.append(Letra3)
            arrayDeLetras.append(Letra1)
            arrayDeLetras.append(Letra2)
            break
        default:
            arrayDeLetras.append(Letra3)
            arrayDeLetras.append(Letra2)
            arrayDeLetras.append(Letra1)
            break
        }
        let letraDireita = arrayDeLetras[0]
        let letraEsquerda = arrayDeLetras[1]
        let letraCentro = arrayDeLetras[2]
        
        let botaoEsquerdo = SKSpriteNode(color: UIColor.lightGrayColor(), size: CGSizeMake((self.frame.size.width*0.3), self.frame.size.height*0.15))
        botaoEsquerdo.anchorPoint = CGPoint(x: 0, y: 0)
        botaoEsquerdo.name = Names.botoesAlternativas.rawValue
        botaoEsquerdo.position = CGPointMake(0, 0)
        botaoEsquerdo.zPosition = Layer.Botoes.rawValue
        self.addChild(botaoEsquerdo)
        
        let botaoEsquerdoLabel = SKLabelNode(text: letraDireita)
        botaoEsquerdoLabel.fontColor = UIColor.blackColor()
        botaoEsquerdoLabel.name = letraDireita
        botaoEsquerdoLabel.zPosition = Layer.Botoes.rawValue
        botaoEsquerdoLabel.position = CGPoint(x: botaoEsquerdo.size.width/2, y: botaoEsquerdo.size.height/2)
        botaoEsquerdo.addChild(botaoEsquerdoLabel)
        
        let botaoDireito = SKSpriteNode(color: UIColor.lightGrayColor(), size: CGSizeMake((self.frame.size.width*0.3), self.frame.size.height*0.15))
        botaoDireito.anchorPoint = CGPoint(x: 1, y: 0)
        botaoDireito.name = Names.botoesAlternativas.rawValue
        botaoDireito.zPosition = Layer.Botoes.rawValue
        botaoDireito.position = CGPointMake(self.size.width, 0)
        self.addChild(botaoDireito)
        
        let botaoDireitoLabel = SKLabelNode(text: letraEsquerda)
        botaoDireitoLabel.fontColor = UIColor.blackColor()
        botaoDireitoLabel.name = letraEsquerda
        botaoDireitoLabel.zPosition = Layer.Botoes.rawValue
        botaoDireitoLabel.position = CGPoint(x: -botaoDireito.size.width/2, y: botaoDireito.size.height/2)
        botaoDireito.addChild(botaoDireitoLabel)
        
        let botaoCentro = SKSpriteNode(color: UIColor.lightGrayColor(), size: CGSizeMake((self.frame.size.width*0.3), self.frame.size.height*0.15))
        botaoCentro.anchorPoint = CGPoint(x: 0.5, y: 0)
        botaoCentro.name = Names.botoesAlternativas.rawValue
        botaoCentro.zPosition = Layer.Botoes.rawValue
        botaoCentro.position = CGPointMake(self.size.width/2, 0)
        self.addChild(botaoCentro)
        
        let botaoCentroLabel = SKLabelNode(text: letraCentro)
        botaoCentroLabel.fontColor = UIColor.blackColor()
        botaoCentroLabel.name = letraCentro
        botaoCentroLabel.zPosition = Layer.Botoes.rawValue
        botaoCentroLabel.position = CGPoint(x: 0, y: botaoCentro.size.height/2)
        botaoCentro.addChild(botaoCentroLabel)
        
        
        arrayDeBotoes.append(botaoCentro)
        arrayDeBotoes.append(botaoDireito)
        arrayDeBotoes.append(botaoEsquerdo)
        
        arrayDeLabelNodes.append(botaoCentroLabel)
        arrayDeLabelNodes.append(botaoDireitoLabel)
        arrayDeLabelNodes.append(botaoEsquerdoLabel)
    }
    
    func addBotaoPlayGame ()
    {
        let button = SKSpriteNode(color: UIColor.blackColor(), size: CGSizeMake(70, 70))
        button.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        button.name = Names.botaoPlayGame.rawValue
        button.zPosition = Layer.Botoes.rawValue
        self.addChild(button)
    }
    
    func addBotaoVoltar ()
    {
        let button = SKSpriteNode(color: UIColor.blackColor(), size: CGSizeMake(70, 70))
        button.position = CGPointMake(10, (self.view?.bounds.size.height)! - 10)
        button.name = Names.botaoPreviousScreen.rawValue
        button.zPosition = Layer.Botoes.rawValue
        self.addChild(button)
    }
    
    func montarBaseDaTela ()
    {

        arrayQueVaiPiscar = montarArray()
        criarBotoes(arrayQueVaiPiscar[acertos][0], Letra2: arrayQueVaiPiscar[acertos][1], Letra3: arrayQueVaiPiscar[acertos][2])
        podePiscar = true

    }
    
    //MARK: ACOES REALIZADAS
    func comecarGame (gameBegan: Bool)
    {
        
        if !gameBegan
        {
            montarBaseDaTela()
            jogoComecou = gameBegan
        }
        
    }
    
    //TODO: FUNCAO GANHOU, PERDEU
    func ganhou ()
    {
        jogoComecou = false
        self.removeAllChildren()
        addBotaoVoltar()
    }
    func perdeu ()
    {
        jogoComecou = false
        acertos = 0
        vidas = 3
        self.physicsWorld.gravity = CGVectorMake(0, -CGFloat(acertos+1) * 0.2)
        self.removeAllChildren()
        addBotaoVoltar()
        addBotaoPlayGame()
    }

    func acertou ()
    {
        acertos++
        
    }
    func errou ()
    {
        vidas--
    }
    func soltaOSom (letraDoSom: String)
    {
        if let path: NSURL = NSBundle.mainBundle().URLForResource(letraDoSom, withExtension: "mp3")
        {
            do {
                try audioPlayer = AVAudioPlayer(contentsOfURL: path)
            } catch _{
                print("NO AUDIO PLAYER")
            }
            audioPlayer?.numberOfLoops = 0
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
        }
        
    }
    
    
    
    func piscarLetra (LetraDoBotao: String)
    {
        for botao in self.children
        {
            if botao.name==Names.botoesAlternativas.rawValue
            {
                if let _ = botao.childNodeWithName(LetraDoBotao)
                {
                    let esperar = SKAction.waitForDuration(0.2)
                    let colorir = SKAction.colorizeWithColor(UIColor.orangeColor(), colorBlendFactor: 1, duration: 0.2)
                    let apagar = SKAction.colorizeWithColor(UIColor.lightGrayColor(), colorBlendFactor: 1, duration: 0.2)
                    let sequencia = SKAction.sequence([esperar,colorir, esperar, apagar])
                    botao.runAction(sequencia)

                }
            }
        }
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches 
        let location = touch.first!.locationInNode(self)
        let node = self.nodeAtPoint(location)
        
        if (node.name == Names.botaoPreviousScreen.rawValue) {
            let gameScene = GameScene(size: self.size)
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(gameScene, transition: transition)
        }
        else if node.name == Names.botaoPlayGame.rawValue
        {
            comecarGame(jogoComecou)
            node.removeFromParent()
        }

        
        for labelNode in arrayDeBotoes
        {
            if acertos > arrayQueVaiPiscar.count
            {
                return
            }
            
            if arrayQueVaiPiscar[acertos].count > correndoSequencia && vezDoJogadorRepetirPadrao
            {
                if let label = labelNode.childNodeWithName(arrayQueVaiPiscar[acertos][correndoSequencia])
                {
                    if labelNode.containsPoint(location)
                    {
                        correndoSequencia++
                        print("certo \(label.name)")
                        
                        if let dicionario = myDictionaries.dicionarioDeLetras[label.name!]
                        {
                            if let acertos = dicionario["acertos"]
                            {
                                myDictionaries.dicionarioDeLetras[label.name!]?.updateValue(acertos + 1, forKey: "acertos")
                            }
                        }
                        let colorirVerde = SKAction.colorizeWithColor(UIColor.greenColor(), colorBlendFactor: 0.7, duration: 0.2)
                        let colorirCinza = SKAction.colorizeWithColor(UIColor.lightGrayColor(), colorBlendFactor: 1.0, duration: 0.2)
                        let esperar = SKAction.waitForDuration(0.2)
                        let sequencia = SKAction.sequence([colorirVerde, esperar, colorirCinza])
                        labelNode.runAction(sequencia)
                        if !(arrayQueVaiPiscar[acertos].count > correndoSequencia)
                        {
                            correndoSequencia = 0
                            acertou()
                            vezDoJogadorRepetirPadrao = false
                            
                        }
                        break
                    }
                    else
                    {
                        errou()
                        
                        for encontrarErrado in arrayDeBotoes
                        {
                            if encontrarErrado.containsPoint(location)
                            {
                                if let nome = encontrarErrado.children[0].name
                                {
                                    if let erro1 = myDictionaries.dicionarioDeLetras[nome]
                                    {
                                        if let erros = erro1["erros"]
                                        {
                                            myDictionaries.dicionarioDeLetras[nome]?.updateValue(erros + 1, forKey: "erros")
                                        }
                                    }
                                    if let erro2 = myDictionaries.dicionarioDeLetras[label.name!]
                                    {
                                        
                                        if let erros = erro2["erros"]
                                        {
                                            myDictionaries.dicionarioDeLetras[label.name!]?.updateValue(erros + 1, forKey: "erros")
                                        }

                                    }
                                }
                                let colorirVerm = SKAction.colorizeWithColor(UIColor.redColor(), colorBlendFactor: 0.7, duration: 0.2)
                                let colorirCinza = SKAction.colorizeWithColor(UIColor.lightGrayColor(), colorBlendFactor: 1.0, duration: 0.2)
                                let esperar = SKAction.waitForDuration(0.2)
                                let sequencia = SKAction.sequence([colorirVerm, esperar, colorirCinza])
                                encontrarErrado.runAction(sequencia)
                                break
                            }
                        }
                    }
                }
            }
            print(myDictionaries.dicionarioDeLetras)

            
        }

        
    }
    func podeRepetirPadrao ()
    {
        vezDoJogadorRepetirPadrao = true

    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
   
        
        if !vezDoJogadorRepetirPadrao  && podePiscar
        {
            if currentTime - tempoAtual > 1 && arrayQueVaiPiscar.count > acertos
            {
                piscarLetra(arrayQueVaiPiscar[acertos][letraCorrente])
                soltaOSom(arrayQueVaiPiscar[acertos][letraCorrente])
                tempoAtual = currentTime
                if letraCorrente + 1 < arrayQueVaiPiscar[acertos].count
                {
                    letraCorrente++
                }
                else
                {
                    letraCorrente = 0
                    tempoAtual = currentTime + 2.6
                    NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(0.5), target: self, selector: "podeRepetirPadrao", userInfo: nil, repeats: false)
                }
            }
            else
            {
            }
            if arrayQueVaiPiscar.count == acertos && podePiscar
            {
                ganhou()
            }
        }
    }
    

}