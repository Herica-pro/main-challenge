//
//  Game2.swift
//  Xprize1App
//
//  Created by Felipe Lukavei Ferreira on 9/9/15.
//  Copyright (c) 2015 Felipe Lukavei Ferreira. All rights reserved.
//

import SpriteKit
import AVFoundation


class Game2: SKScene {
    var jogoComecou = false
    var acertos = 0
    var montouOsBotoes = false
    var arrayMontado = Array<[String]>()
    var roundOne = true
    var vidas = 3
    var audioPlayer: AVAudioPlayer!
    var arrayDeBotoes = Array<SKSpriteNode>()

    enum Layer: CGFloat {
        case Background
        case Player
        case Botoes
    }
    enum Names: String {
        case botaoPreviousScreen = "botaoPreviousScreen"
        case backGround = "backGround"
        case Player = "player"
        case botoesAlternativas = "btAlternativas"
        case botaoPlayGame = "playGameButton"
    }
    
    //MARK: ENTRADA DE DADOS
    
    
    func montarArray() -> Array<[String]>
    {
        //TODO: ENTRADA DE ARRAY DE LETRAS CONHECIDAS PELO USUARIO -> SISTEMA DE CARTAS COLECIONAVEIS
        var arrayDeArrays = Array<[String]>()

        arrayDeArrays.append(sortearLetras())
        arrayDeArrays.append(sortearLetras())
        arrayDeArrays.append(sortearLetras())
        arrayDeArrays.append(sortearLetras())
        arrayDeArrays.append(sortearLetras())
        arrayDeArrays.append(sortearLetras())
        arrayDeArrays.append(sortearLetras())

        print(arrayDeArrays)
        return arrayDeArrays
        
    }
    func sortearLetras () -> [String]
    {
        let letrasConhecidas = myDictionaries.letrasConhecidas

        let letra1 = Int(arc4random_uniform(UInt32(letrasConhecidas.count)))
        var letra2 = Int(arc4random_uniform(UInt32(letrasConhecidas.count)))
        while letra2 == letra1
        {
            letra2 = Int(arc4random_uniform(UInt32(letrasConhecidas.count)))
        }
        let array = [letrasConhecidas[letra1], letrasConhecidas[letra2]]

        return array
    }
    //MARK: MONTAR TELA
    override func didMoveToView(view: SKView)
    {
        self.backgroundColor = SKColor(red: 0.15, green:0.15, blue:0.3, alpha: 1.0)
        addBotaoVoltar()
        addBotaoPlayGame()
        self.physicsWorld.gravity = CGVectorMake(0, -CGFloat(acertos+1) * 0.2)
    }
    
    func criarBotoes(LetraCorreta: String, LetraErrada: String)
    {
        var letra1 = String()
        var letra2 = String()
        if arc4random_uniform(2) > 0 //possibilidade 1
        {
            letra1 = LetraCorreta
            letra2 = LetraErrada
        }
        else //possibilidade 2
        {
            letra1 = LetraErrada
            letra2 = LetraCorreta
        }
        print("correta: \(LetraCorreta)")
        if roundOne
        {
            let botaoEsquerdo = SKSpriteNode(color: UIColor.lightGrayColor(), size: CGSizeMake((self.frame.size.width*0.49), self.frame.size.height*0.15))
            botaoEsquerdo.anchorPoint = CGPoint(x: 0, y: 0)
            botaoEsquerdo.name = Names.botoesAlternativas.rawValue
            botaoEsquerdo.position = CGPointMake(0, 0)
            botaoEsquerdo.zPosition = Layer.Botoes.rawValue
            self.addChild(botaoEsquerdo)

            let botaoEsquerdoLabel = SKLabelNode(text: letra1)
            botaoEsquerdoLabel.fontColor = UIColor.blackColor()
            botaoEsquerdoLabel.name = LetraCorreta
            botaoEsquerdoLabel.zPosition = Layer.Botoes.rawValue
            botaoEsquerdoLabel.position = CGPoint(x: botaoEsquerdo.size.width/2, y: botaoEsquerdo.size.height/2)
            botaoEsquerdo.addChild(botaoEsquerdoLabel)

            let botaoDireito = SKSpriteNode(color: UIColor.lightGrayColor(), size: CGSizeMake((self.frame.size.width*0.49), self.frame.size.height*0.15))
            botaoDireito.anchorPoint = CGPoint(x: 1, y: 0)
            botaoDireito.name = Names.botoesAlternativas.rawValue
            botaoDireito.zPosition = Layer.Botoes.rawValue
            botaoDireito.position = CGPointMake(self.size.width, 0)
            self.addChild(botaoDireito)

            let botaoDireitoLabel = SKLabelNode(text: letra2)
            botaoDireitoLabel.fontColor = UIColor.blackColor()
            botaoDireitoLabel.name = LetraCorreta
            botaoDireitoLabel.zPosition = Layer.Botoes.rawValue
            botaoDireitoLabel.position = CGPoint(x: -botaoDireito.size.width/2, y: botaoDireito.size.height/2)
            botaoDireito.addChild(botaoDireitoLabel)
            
            arrayDeBotoes.append(botaoEsquerdo)
            arrayDeBotoes.append(botaoDireito)
        }
        else
        {
            for node in self.children
            {
                if node.name == Names.botoesAlternativas.rawValue
                {
                    if let labelNode = node.children.first as? SKLabelNode
                    {
                        if labelNode.text == labelNode.name
                        {
                            labelNode.text = letra1
                            labelNode.name = LetraCorreta
                        }
                        else
                        {
                            labelNode.text = letra2
                            labelNode.name = LetraCorreta
                        }
                    }
                }
                
            }

        }
    }
    
    func addBotaoPlayGame ()
    {
        let button = SKSpriteNode(color: UIColor.blackColor(), size: CGSizeMake(70, 70))
        button.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        button.name = Names.botaoPlayGame.rawValue
        button.zPosition = Layer.Botoes.rawValue
        self.addChild(button)
    }
    func addBotaoVoltar ()
    {
        let button = SKSpriteNode(color: UIColor.blackColor(), size: CGSizeMake(70, 70))
        button.position = CGPointMake(10, (self.view?.bounds.size.height)! - 10)
        button.name = Names.botaoPreviousScreen.rawValue
        button.zPosition = Layer.Botoes.rawValue
        self.addChild(button)
    }
    func montarBaseDaTela ()
    {
        if arrayMontado.count == 0 && !roundOne
        {
            ganhou()
        }
        
        if !roundOne
        {
            criarBotoes(arrayMontado[0][0], LetraErrada: arrayMontado[0][1])
            soltaOSom(arrayMontado[0][0])
            arrayMontado.removeAtIndex(0)

        }
        if roundOne && jogoComecou
        {
            arrayMontado = montarArray()
            criarBotoes(arrayMontado[0][0], LetraErrada: arrayMontado[0][1])
            soltaOSom(arrayMontado[0][0])
            arrayMontado.removeAtIndex(0)


            roundOne = false
        }
    }
    func soltaOSom (letraDoSom: String)
    {
        if let path: NSURL = NSBundle.mainBundle().URLForResource(letraDoSom, withExtension: "mp3")
        {
            do {
                try audioPlayer = AVAudioPlayer(contentsOfURL: path)
            } catch _{
                print("NO AUDIO PLAYER")
            }
            audioPlayer?.numberOfLoops = 0
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
        }
        
    }

    
    //MARK: COMEÇAR PARTIDA / Ganhou e Perdeu
    //CRIAR QUADRADOS COM AS CORES
    func comecarGame (gameBegan: Bool)
    {
        if !gameBegan
        {
            let personagem = SKSpriteNode(color: UIColor.yellowColor(), size: CGSizeMake(30, 30))
            personagem.position = CGPointMake(self.size.width/2, self.size.height*18/20)
            personagem.zPosition = Layer.Player.rawValue
            personagem.name = Names.Player.rawValue
            personagem.color = UIColor.yellowColor()
            personagem.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(30, 0.1))
            personagem.anchorPoint = CGPoint(x:0.5, y:1.0)
            personagem.physicsBody?.mass = 1
            personagem.physicsBody?.dynamic = true
            personagem.physicsBody?.affectedByGravity = true
            self.addChild(personagem)
            jogoComecou = !gameBegan
        }
        
    }
    //TODO: FUNCAO GANHOU
    func ganhou ()
    {
        roundOne = true
        jogoComecou = false
        self.removeAllChildren()
        print(self.children)
        addBotaoVoltar()
    }
    func perdeu ()
    {
        jogoComecou = false
        acertos = 0
        montouOsBotoes = false
        arrayMontado = Array<[String]>()
        roundOne = true
        vidas = 3
        self.physicsWorld.gravity = CGVectorMake(0, -CGFloat(acertos+1) * 0.2)
        self.removeAllChildren()
        addBotaoVoltar()
        addBotaoPlayGame()
    }
    
    //MARK: TOUCHING
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches 
        let location = touch.first!.locationInNode(self)
        let node = self.nodeAtPoint(location)
        for node in arrayDeBotoes
        {
            if node.containsPoint(location)
            {
                if let labelNode = node.children.first as? SKLabelNode
                {
                    if labelNode.text == labelNode.name
                    {
                        if let dicionario = myDictionaries.dicionarioDeLetras[labelNode.name!]
                        {
                            if let acertos = dicionario["acertos"]
                            {
                                myDictionaries.dicionarioDeLetras[labelNode.name!]?.updateValue(acertos + 1, forKey: "acertos")
                            }
                        }
//                            print(myDictionaries.dicionarioDeLetras)
                        
                        montarBaseDaTela()
                        let colorirVerde = SKAction.colorizeWithColor(UIColor.greenColor(), colorBlendFactor: 1, duration: 0.2)
                        let colorirCinza = SKAction.colorizeWithColor(UIColor.lightGrayColor(), colorBlendFactor: 1.0, duration: 0.2)
                        let esperar = SKAction.waitForDuration(0.2)
                        let sequencia = SKAction.sequence([colorirVerde, esperar, colorirCinza])
                        node.runAction(sequencia)
                        if let personagem = self.childNodeWithName(Names.Player.rawValue)
                        {
                            personagem.physicsBody?.applyForce(CGVector(dx: 0, dy: 5000 + acertos * 1000))
                        }
                        acertos++
                        self.physicsWorld.gravity = CGVectorMake(0, -CGFloat(acertos+1) * 0.2)
                        
                    }
                    else
                    {
                        vidas--
                        for nodeErrou in arrayDeBotoes
                        {
                            if let filho = nodeErrou.children.first as? SKLabelNode
                            {
                                print("letra: \(filho.text!)")
                                if let dicionario = myDictionaries.dicionarioDeLetras[filho.text!]
                                {
                                    if let erros = dicionario["erros"]
                                    {
                                        myDictionaries.dicionarioDeLetras[filho.text!]?.updateValue(erros + 1, forKey: "erros")
                                    }
                                    print(dicionario)

                                }

                            }
                        }
                        print("\(myDictionaries.dicionarioDeLetras)")
                        
                        montarBaseDaTela()
                        
                    }
                }
            
            }
        }

        if !jogoComecou
        {
            if node.name == Names.botaoPlayGame.rawValue
            {
                comecarGame(jogoComecou)
                montarBaseDaTela()
                node.removeFromParent()
            }
            
        }

        if (node.name == Names.botaoPreviousScreen.rawValue) {
            let gameScene = GameScene(size: self.size)
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(gameScene, transition: transition)
        }

        
    }
    override func update(currentTime: NSTimeInterval) {
        if (self.childNodeWithName(Names.Player.rawValue)?.position.y < -5 || vidas == 0) && jogoComecou
        {
          perdeu()
        }
        
    }
}