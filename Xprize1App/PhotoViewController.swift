//
//  ViewController.swift
//  Xprize1App
//
//  Created by Felipe Lukavei Ferreira on 9/21/15.
//  Copyright © 2015 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit
import OpencvSwift

class PhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var viewDaFoto: UIImageView!
    var imagemFinal = UIImage()
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imagePicker.delegate = self
        
        let imagem = CvMat(imageNamed: "background.jpg")
        Cv3.cvtColor(imagem, output: imagem, type: CvColor.BGRA2BGR)
        let imagemRecortada = imagem.crop(CGRect(x: 300, y: 0, width: 600, height: 600))
        viewDaFoto.image = imagemRecortada.toUIImage()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonTakePhoto(sender: AnyObject) {

        imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        imagePicker.cameraDevice = UIImagePickerControllerCameraDevice.Front
        self.presentViewController(imagePicker, animated: true, completion: nil)
        print("1")
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let image: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        imagemFinal = image
        self.viewDaFoto.image = image
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func BotãoReturn(sender: AnyObject) {
        saveImage(imagemFinal, path: fileInDocumentsDirectory("/imagemPersonagem"))
        performSegueWithIdentifier("ReturnToGameView", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    func saveImage (image: UIImage, path: String ) -> Bool{
        
        let pngImageData = UIImagePNGRepresentation(image)
        //let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
        if let imagem = pngImageData
        {
            let result = pngImageData!.writeToFile(path, atomically: true)
            return result
        }
        return false
        
    }
    func loadImageFromPath(path: String) -> UIImage? {
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
            print("missing image at: (path)")
        }
        print("\(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
        
    }
    func documentsDirectory() -> String {
        let documentsFolderPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as! String
        return documentsFolderPath
    }
    // Get path for a file in the directory
    
    func fileInDocumentsDirectory(filename: String) -> String {
        return documentsDirectory().stringByAppendingString(filename)
    }

}
