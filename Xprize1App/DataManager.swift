//
//  DataManager.swift
//  TopApps
//
//  Created by Dani Arnaout on 9/2/14.
//  Edited by Eric Cerney on 9/27/14.
//  Copyright (c) 2014 Ray Wenderlich All rights reserved.
//

import Foundation

struct myDictionaries
{
    static var dicionarioDeLetras = [
        "Q":["erros":0,"acertos":0],
        "W":["erros":0,"acertos":0],
        "E":["erros":0,"acertos":0],
        "R":["erros":0,"acertos":0],
        "T":["erros":0,"acertos":0],
        "Y":["erros":0,"acertos":0],
        "U":["erros":0,"acertos":0],
        "I":["erros":0,"acertos":0],
        "O":["erros":0,"acertos":0],
        "P":["erros":0,"acertos":0],
        "A":["erros":0,"acertos":0],
        "S":["erros":0,"acertos":0],
        "D":["erros":0,"acertos":0],
        "F":["erros":0,"acertos":0],
        "G":["erros":0,"acertos":0],
        "H":["erros":0,"acertos":0],
        "J":["erros":0,"acertos":0],
        "K":["erros":0,"acertos":0],
        "L":["erros":0,"acertos":0],
        "Z":["erros":0,"acertos":0],
        "X":["erros":0,"acertos":0],
        "C":["erros":0,"acertos":0],
        "V":["erros":0,"acertos":0],
        "B":["erros":0,"acertos":0],
        "N":["erros":0,"acertos":0],
        "M":["erros":0,"acertos":0]]
    
    static var letrasConhecidas = ["A","B","C"]
    
    static var palavras = [
        "zebra":["Z","E","B","R","A"],
        "elefante":["E","L","E","F","A","N","T","E"],
        "arrayPadrao":["zebra","elefante"],
        "arrayEmUso":["zebra","elefante"]
    ]


}

class DataManager {
    
   
  
    enum keysUserData : String
    {
        case palavras = "palavrasPadrao"
        case palavrasEmUso = "palavrasEmUso"
        case alfabeto = "alfabeto"
        case letrasConhecidas = "letrasConhecidas"
    }
    
        
  class func getTopAppsDataFromFileWithSuccess(success: ((data: NSData) -> Void)) {
    //1
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
      //2
      let filePath = NSBundle.mainBundle().pathForResource("xprize",ofType:"json")
   
      var readError:NSError?
      do {
        let data = try NSData(contentsOfFile:filePath!,
          options: NSDataReadingOptions.DataReadingUncached)
        success(data: data)
      } catch let error as NSError {
        readError = error
      } catch {
        fatalError()
      }
        
    })
    
  }
    
    
    
    
    class func saveUserData ()
    {
        //ALFABETO
        NSUserDefaults.standardUserDefaults().setObject( myDictionaries.dicionarioDeLetras, forKey: keysUserData.alfabeto.rawValue)
    
        //PALAVRAS
        NSUserDefaults.standardUserDefaults().setObject( myDictionaries.palavras, forKey: keysUserData.palavras.rawValue)
        
        //LETRAS CONHECIDAS
        NSUserDefaults.standardUserDefaults().setObject(myDictionaries.letrasConhecidas, forKey: keysUserData.letrasConhecidas.rawValue)
    }
    
    
    class func getUserData (DictionaryName: String, SubdivisaoDoDicionario: String?, subSubDivisao: String?) -> AnyObject?
    {
        if let name = NSUserDefaults.standardUserDefaults().objectForKey(DictionaryName)
        {
            if let key = SubdivisaoDoDicionario
            {
                if let objetoDoNome = name.objectForKey(key)
                {
                    if let subKey = subSubDivisao
                    {
                        if let acertos = objetoDoNome.objectForKey(subKey)
                        {
                            return acertos //as? [String:[String:Int]]
//                            return acertos as? [String : [String : Int]]
                        }
                    }
                    else
                    {
                        return objetoDoNome //as? [String:Int]
//                        return objetoDoNome as? [String : [String : Int]]
                    }
                }
            }

            return name// as? Int
//            return name as? [String : [String : Int]]
        }
        return nil
    }
}