//
//  GameViewController.swift
//  Xprize1App
//
//  Created by Felipe Lukavei Ferreira on 9/9/15.
//  Copyright (c) 2015 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit
import SpriteKit

extension SKNode {
    class func unarchiveFromFile(file : String) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            let sceneData = try! NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe)
            let archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as! GameScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}

extension UIImage {
    public func imageRotatedByDegrees(degrees: CGFloat, flip: Bool) -> UIImage {
        let radiansToDegrees: (CGFloat) -> CGFloat = {
            return $0 * (180.0 / CGFloat(M_PI))
        }
        let degreesToRadians: (CGFloat) -> CGFloat = {
            return $0 / 180.0 * CGFloat(M_PI)
        }
        
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView(frame: CGRect(origin: CGPointZero, size: size))
        let t = CGAffineTransformMakeRotation(degreesToRadians(degrees));
        rotatedViewBox.transform = t
        let rotatedSize = rotatedViewBox.frame.size
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap = UIGraphicsGetCurrentContext()
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        CGContextTranslateCTM(bitmap, rotatedSize.width / 2.0, rotatedSize.height / 2.0);
        
        //   // Rotate the image context
        CGContextRotateCTM(bitmap, degreesToRadians(degrees));
        
        // Now, draw the rotated/scaled image into the context
        var yFlip: CGFloat
        
        if(flip){
            yFlip = CGFloat(-1.0)
        } else {
            yFlip = CGFloat(1.0)
        }
        
        CGContextScaleCTM(bitmap, yFlip, -1.0)
        CGContextDrawImage(bitmap, CGRectMake(-size.width / 2, -size.height / 2, size.width, size.height), CGImage)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}


class GameViewController: UIViewController {

    var travarEntradaGeral = Bool()
    let photoController = PhotoViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK: CONTROLE DE HISTORIA
        loadScene(travarEntradaGeral)

        
    }

    func loadScene (travarEntrada:Bool)
    {
        travarEntradaGeral = travarEntrada
        if travarEntradaGeral != true
        {
            if let palavra = DataManager.getUserData(DataManager.keysUserData.palavrasEmUso.rawValue, SubdivisaoDoDicionario: nil, subSubDivisao: nil) as? [String: Array<String>]
            {
            myDictionaries.palavras = palavra
            }
            if let letraConhecida = DataManager.getUserData(DataManager.keysUserData.letrasConhecidas.rawValue, SubdivisaoDoDicionario: nil, subSubDivisao: nil) as? [String]
            {
            myDictionaries.letrasConhecidas = letraConhecida
            }
            if let dicionarioLetras = DataManager.getUserData(DataManager.keysUserData.alfabeto.rawValue, SubdivisaoDoDicionario: nil, subSubDivisao: nil) as? [String : Dictionary < String,Int >]
            {
            myDictionaries.dicionarioDeLetras = dicionarioLetras
            }

//            print(myDictionaries.dicionarioDeLetras)
            
            if let scene = GameScene.unarchiveFromFile("GameScene") as? GameScene
            {
                // Configure the view.
                let skView = self.view as! SKView
                skView.showsFPS = true
                skView.showsNodeCount = true
                skView.showsFields = true
                skView.showsPhysics = true
                /* Sprite Kit applies additional optimizations to improve rendering performance */
                skView.ignoresSiblingOrder = true
                skView.bounds.size = UIScreen.mainScreen().bounds.size
                /* Set the scale mode to scale to fit the window */
                scene.size = UIScreen.mainScreen().bounds.size
                scene.scaleMode = .AspectFill
                scene.physicsWorld.gravity = CGVectorMake(0.0, -9.81)
                skView.presentScene(scene)
            }
            
            travarEntradaGeral = true

        }
        else
        {
            chamarPhotoView(true)
        }
    }
    
    func chamarPhotoView (valorBooleano: Bool)
    {
        travarEntradaGeral = valorBooleano
        self.presentViewController(photoController, animated: true, completion: nil)
    }
    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return UIInterfaceOrientationMask.AllButUpsideDown
        } else {
            return UIInterfaceOrientationMask.All
        }
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
